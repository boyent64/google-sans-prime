### 2024-01-24
- Updated GS fonts
- Updated OMF template

[Full Changelog](https://gitlab.com/nongthaihoang/google-sans-prime/-/commits/master)
